# Project statistiek

- **Deadline 24 april**
- Alle commando's in een script
- Verslag is best volgens de opsplitsing 'methode', 'discussie' en 'resultaten' en maximaal 4 pagina's


## Clustering

Vraag: zijn er a priori groepen aanwezig in de data?
- gewone data
- gestandaardiseerde data (met `scale()`)

## Multivariate normaliteit

De analyse in `R` zal veel langer zijn dan wat er op papier staat! Enkel relevante informatie en grafieken vermelden in verslag.
- volledige dataset
- binnen de drie rassen

## Principaalcomponentanalyse

Interpretatie van de PC's zal moeilijk zijn, maar doe toch een poging om er een aan te geven.

## Classificatie

Volgens de drie-stapsmethode met trainings-, validatie- en testset.
Zie hiervoor ook nog eens de video van de oefenzitting over classificatie.
